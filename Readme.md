# Profissa API

## Base URL

```
https://profissa-server.herokuapp.com
```

## Base Header

```
Content-Type: application/json
```

# Endpoints

##### Body

The api doesn't have a standard data structure,
for now, any json structure can be sent with only
some mandatory data that will be flagged with *

## POST /login

##### body


```json
{
  *"email": "teste",
  *"password": "1234"
}
```

## POST /register

##### body

###### example:
```json
{ 
   *"email": "test@test.com",
   *"password": "1234",
    "name": "name",
    "user": "user",
}
```

## PUT /users/:id

Just send what you want to change.

##### body

###### example:
```json
{
    "name": "name",
    "user": "user",
}
```

## GET /users

brings all users

## GET /users/:id

brings the user with an id that was given

## DELETE /users/:id

delete the user with an id that was given

# Feedbacks

##### Header

this header is required for all requests

```json
    {
      "Authorization": "Bearer " + "validToken"
    }
```

## GET /feedbacks

brings all feedbacks

## GET /feedbacks/:id

brings the feedback with an id that was given

## POST /feedbacks

create a feedback

##### body

###### example:


```json
 {
    "creatorId": 2,
    "creator":"Object with all creator information",
    "receiverId": 3,
    "receiverType": "profissa",
    "feedback": "the best Profissa",
    "stars": 5,
    "id": 14
  }
```

## DELETE feedbacks/:id

delete the feedback with an id that was given


# Schedules

##### Header

this header is required for all requests

```json
    {
      "Authorization": "Bearer " + "validToken"
    }
```

## GET /schedules

brings all schedules

## GET /schedules/:id

brings the schedule with an id that was given

## POST /schedules

create a schedule

##### body

###### example:


```json
 {
    "creatorId": 2,
    "creator":"Object with all creator information",
    "receiverId": 3,
    "receiverType": "profissa",
    "schedule": "the best Profissa",
    "stars": 5,
    "id": 14
  }
```

## DELETE schedules/:id

delete the schedule with an id that was given